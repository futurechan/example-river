import org.elasticsearch.common.inject.AbstractModule;
import org.elasticsearch.river.River;

/**
 * Created by joshchan on 4/18/2014.
 */
public class ModuleImpl extends AbstractModule {
    @Override
    protected void configure() {
        bind(River.class).to(RiverImpl.class).asEagerSingleton();
    }
}
