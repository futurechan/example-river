import org.elasticsearch.client.Client;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.xcontent.support.XContentMapValues;
import org.elasticsearch.river.AbstractRiverComponent;
import org.elasticsearch.river.River;
import org.elasticsearch.river.RiverName;
import org.elasticsearch.river.RiverSettings;

import java.util.Map;

import static org.elasticsearch.common.collect.Maps.newHashMap;

/**
 * Created by joshchan on 4/18/2014.
 */
public class RiverImpl extends AbstractRiverComponent implements River {

    public final static String TYPE = "river";

    @Inject
    protected RiverImpl(RiverName riverName, RiverSettings settings, Client client) {
        super(riverName, settings);

        logger.info("here");

        Map<String, Object> mySettings = newHashMap();
        if (settings.settings().containsKey(TYPE)) {
            mySettings = (Map<String, Object>) settings.settings().get(TYPE);
        }
        String strategy = XContentMapValues.nodeStringValue(mySettings.get("strategy"), "cphs");
        logger.info("strategy: " + strategy);
    }

    @Override
    public void start() {

    }

    @Override
    public void close() {

    }
}
