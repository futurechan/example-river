import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.plugins.AbstractPlugin;
import org.elasticsearch.river.RiversModule;

/**
 * Created by joshchan on 4/18/2014.
 */
public class PluginImpl extends AbstractPlugin {

    @Inject
    public PluginImpl() {
    }

    @Override
    public String name() {
        return "river-impl";
    }

    @Override
    public String description() {
        return "Example River plugin";
    }

    public void onModule(RiversModule module) {
        module.registerRiver(RiverImpl.TYPE, ModuleImpl.class);
        //client.admin().indices().prepareDeleteMapping("_river").setType(riverName.name()).execute();
    }


}
